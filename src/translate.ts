// Import all directives
import {TranslateDirective} from './translate/translate.directive';
import {TranslateComponent} from './translate/translate.component';

// Export all directives
export * from './translate/translate.directive';
export * from './translate/translate.component';

// Export convenience property
export const TRANSLATE: any[] = [
  TranslateDirective,
  TranslateComponent
];
