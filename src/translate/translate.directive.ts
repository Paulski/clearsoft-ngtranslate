import {Directive, ElementRef, Input, Renderer} from '@angular/core';

@Directive({
  selector: '[mfWord]',
  exportAs: 'mfTranslate'
})

export class TranslateDirective {

  @Input() public mfWord:any[] = [];

  constructor(private el: ElementRef, renderer: Renderer) {
    renderer.setElementStyle(el.nativeElement, 'backgroundColor', 'yellow');
    // console.log(this);
    // this.testAlert(this);
  }

}
