import {Component, ElementRef, Input, Renderer, OnInit} from '@angular/core';

@Component({
  selector: 'translate',
  template: `
    {{replaceOn}}
  `
})
export class TranslateComponent implements OnInit{

  public object: any;
  @Input("mfWord") private mfWord: string;

  public replaceOn: string = 'undefined_translate';

  constructor() {}

  ngOnInit()
  {
    this.replaceOn = this.mfWord;
  }

}