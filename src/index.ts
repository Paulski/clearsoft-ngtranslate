import {TRANSLATE} from './translate';

export * from './translate';

export default {
  directives: [TRANSLATE]
}
